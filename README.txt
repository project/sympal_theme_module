## Readme for sympal_theme_module

## About
Utility module for the sympal theme. Contains blocks, install files and such for the symapl theme.
See more at http://drupal.org/project/sympal_theme

## credits
By Bèr Kessels, webschuur.com ber@webschuur.com
For
First released for Drupal 5.x
[Homepage](http://drupal.org/projects)
                                     _         _
     ___ _   _ _ __ ___  _ __   __ _| |  _ __ | |
    / __| | | | '_ ` _ \| '_ \ / _` | | | '_ \| |
    \__ \ |_| | | | | | | |_) | (_| | |_| | | | |
    |___/\__, |_| |_| |_| .__/ \__,_|_(_)_| |_|_|
         |___/          |_|

 -readme written in markdown-